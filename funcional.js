class Producto {
  constructor(
    clave,
    descripcion,
    precio,
    clasificacion,
    existencia,
    existenciaMinima,
    existenciaMaxima
  ) {
    this.clave = clave;
    this.descripcion = descripcion;
    this.precio = precio;
    this.clasificacion = clasificacion;
    this.existencia = existencia;
    this.existenciaMinima = existenciaMinima;
    this.existenciaMaxima = existenciaMaxima;
  }
}

// Simulación de carga de productos desde un DAO (simulado)
function cargarProductosDesdeDAO() {
  const productos = [];
  for (let i = 0; i < 80; i++) {
    productos.push(
      new Producto(
        i,
        `Producto ${i}`,
        10.0 + i,
        `Clasificación ${i % 5}`,
        i * 2,
        5,
        100
      )
    );
  }
  return productos;
}

// Problema 1: Número de productos con existencia mayor a 20
const productos = cargarProductosDesdeDAO();
const numProductosExistenciaMayor20 = productos.filter(
  (p) => p.existencia > 20
).length;
console.log(
  "Número de productos con existencia mayor a 20:",
  numProductosExistenciaMayor20
);

// Problema 2: Número de productos con existencia menor a 15
const numProductosExistenciaMenos15 = productos.filter(
  (p) => p.existencia < 15
).length;
console.log(
  "Número de productos con existencia menor a 15:",
  numProductosExistenciaMenos15
);

// Problema 3: Lista de productos con la misma clasificación y precio mayor 15.50
const productosClasificacionYPrecio = productos.filter(
  (p) => p.clasificacion === "Clasificación 0" && p.precio > 15.5
);
console.log(
  "Lista de productos con la misma clasificación y precio mayor 15.50:"
);
console.log(productosClasificacionYPrecio);

// Problema 4: Lista de productos con precio mayor a 20.30 y menor a 45.00
const productosPrecioMayor20Menor45 = productos.filter(
  (p) => p.precio > 20.3 && p.precio < 45.0
);
console.log("Lista de productos con precio mayor a 20.30 y menor a 45.00:");
console.log(productosPrecioMayor20Menor45);

// Problema 5: Número de productos agrupados por su clasificación
const productosAgrupadosPorClasificacion = productos.reduce((acc, p) => {
  acc[p.clasificacion] = (acc[p.clasificacion] || 0) + 1;
  return acc;
}, {});
console.log("Número de productos agrupados por su clasificación:");
console.log(productosAgrupadosPorClasificacion);
